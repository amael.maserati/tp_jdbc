import java.sql.*;

public class ConnexionMySQL {
	private Connection mysql=null;
	private boolean connecte=false;
	public ConnexionMySQL() throws ClassNotFoundException{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch (ClassNotFoundException ex) { 
			System.out.println("Driver non trouvé");
			mysql = null;
			return;
		}
			
	}

	public void connecter(String nomServeur, String nomBase, String nomLogin, String motDePasse) throws SQLException {
		// si tout c'est bien passé la connexion n'est plus nulle
		this.connecte=this.mysql!=null;
		Connection c; 
		try {
			c = DriverManager.getConnection(
				"jdbc:mysql://servinfo-mariadb:3306/"+nomBase,
			nomLogin,motDePasse);
			this.mysql = c;
			this.connecte = true;
		}
		catch (SQLException ex) {
			System.out.println("Msg:" + ex.getMessage()+ex.getErrorCode());
		}
	}
	public void close() throws SQLException {
		// fermer la connexion
		this.connecte=false;
	}

    	public boolean isConnecte() { return this.connecte;}
	public Statement createStatement() throws SQLException {
		return this.mysql.createStatement();
	}

	public PreparedStatement prepareStatement(String requete) throws SQLException{
		return this.mysql.prepareStatement(requete);
	}
	
}
