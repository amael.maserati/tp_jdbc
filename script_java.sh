#!/bin/bash
$(javac --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls *.java)
$(java -cp .:/usr/share/java/mariadb-java-client.jar --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls TestJDBC)
