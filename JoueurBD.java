import java.sql.*;
import java.util.ArrayList;

public class JoueurBD {
	ConnexionMySQL laConnexion;
	Statement st;
	ResultSet resultats;
	JoueurBD(ConnexionMySQL laConnexion){
		this.laConnexion=laConnexion;
		this.resultats = null;
	}

	int maxNumJoueur() throws SQLException{
		String requete = "SELECT max(numJoueur) FROM JOUEUR";
		this.st = this.laConnexion.createStatement();
		this.resultats = this.st.executeQuery(requete);
		this.resultats.next();
		int rs = this.resultats.getInt(1);
		this.resultats.close();
		return rs;
	}


	int insererJoueur( Joueur j) throws  SQLException{
		j.setIdentifiant(this.maxNumJoueur() + 1);
		PreparedStatement r = this.laConnexion.prepareStatement("insert into JOUEUR values (?,?,?,?,?,?)");
		r.setInt(1, j.getIdentifiant());
		r.setString(2, j.getPseudo());
		r.setString(3, j.getMotdepasse());
		r.setString(4, j.isAbonne() ? "O" : "N");
		r.setString(5, String.valueOf(j.getMain()));
		r.setInt(6, j.getNiveau());
		r.executeUpdate();
		return j.getIdentifiant();
	}


	void effacerJoueur(int num) throws SQLException {
		this.laConnexion.prepareStatement("delete from JOUEUR where numJoueur ="+num);
	}

    	void majJoueur(Joueur j)throws SQLException{
			String requete = "UPDATE JOUEUR SET pseudo = ?, motdepasse = ?, abonne = ?, main = ?, niveau = ? where numJoueur = ?";
			PreparedStatement r = this.laConnexion.prepareStatement(requete);
			r.setString(1, j.getPseudo());
			r.setString(2, j.getMotdepasse());
			r.setString(3, j.isAbonne() ? "O" : "N");
			r.setString(4, String.valueOf(j.getMain()));
			r.setInt(5, j.getNiveau());
			r.setInt(6, j.getIdentifiant());
			r.executeUpdate();
    	}

    	Joueur rechercherJoueurParNum(int num)throws SQLException{
    		String requete = "SELECT * from JOUEUR where numJoueur ="+num;
			Statement s = this.laConnexion.createStatement();
			ResultSet r = s.executeQuery(requete);
			if (!r.next()) { throw new SQLException("Joueur non trouvé"); }
			r.next();
			return new Joueur(r.getInt("numJoueur"), r.getString("pseudo"), r.getString("motdepasse"), r.getString("abonne").charAt(0) == 'O' ? true : false, r.getString("main").charAt(0), r.getInt("niveau"));
    	}

	ArrayList<Joueur> listeDesJoueurs() throws SQLException{
		ArrayList<Joueur> liste = new ArrayList<Joueur>();
		String requete = "SELECT * from JOUEUR";
		Statement s = this.laConnexion.createStatement();
		ResultSet r = s.executeQuery(requete);
		while (r.next()) {
			liste.add(new Joueur(r.getInt("numJoueur"), r.getString("pseudo"), r.getString("motdepasse"), r.getString("abonne").charAt(0) == 'O' ? true : false, r.getString("main").charAt(0), r.getInt("niveau")));
		}
		return liste;
	}
	
	String rapportMessage() throws SQLException{
		//  va indiquer pour chaque joueur et chaque jour le nombre de message qu’il a envoyés
		String requete = "SELECT pseudo, dateEnvoi, count(*) from MESSAGE, JOUEUR where MESSAGE.numJoueur = JOUEUR.numJoueur group by pseudo, dateEnvoi";
		Statement s = this.laConnexion.createStatement();
		ResultSet r = s.executeQuery(requete);
		String res = "";
		while (r.next()) {
			res += r.getString("pseudo") + " " + r.getString("dateEnvoi") + " " + r.getInt("count(*)") + "\n";
		}
		return res; 
	}
	
	String rapportMessageComplet() throws SQLException{
		// va indiquer pour chaque joueur la liste des messages qu’il a reçus classés par ordre chronologique
		String requete = "SELECT pseudo, dateEnvoi, contenu from MESSAGE, JOUEUR where MESSAGE.numJoueur = JOUEUR.numJoueur order by pseudo, dateEnvoi";
		Statement s = this.laConnexion.createStatement();
		ResultSet r = s.executeQuery(requete);
		String res = "";
		while (r.next()) {
			res += r.getString("pseudo") + " " + r.getString("dateEnvoi") + " " + r.getString("contenu") + "\n";
		}
		return res;
   	}
}
